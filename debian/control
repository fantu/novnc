Source: novnc
Section: web
Priority: optional
Maintainer: Debian OpenStack <team+openstack@tracker.debian.org>
Uploaders:
 Thomas Goirand <zigo@debian.org>,
 Michal Arbet <michal.arbet@ultimum.io>,
Build-Depends:
 debhelper-compat (= 10),
 dh-python,
 openstack-pkg-tools,
 python3-all,
Build-Depends-Indep:
 gettext,
 node-po2json,
 python3-greenlet,
Standards-Version: 4.1.3
Vcs-Browser: https://salsa.debian.org/openstack-team/third-party/novnc
Vcs-Git: https://salsa.debian.org/openstack-team/third-party/novnc.git
Homepage: https://github.com/novnc/noVNC

Package: novnc
Architecture: all
Depends:
 adduser,
 net-tools,
 python3-novnc,
 python3-numpy,
 websockify,
 ${misc:Depends},
 ${python3:Depends},
 ${shlibs:Depends},
Suggests:
 python-nova,
Description: HTML5 VNC client - daemon and programs
 noVNC is a HTML5 VNC (WebSockets, Canvas) with encryption (wss://) support
 client that runs well in any modern browser including mobile browsers
 (iPhone/iPad and Android). More than 16 companies/projects have integrated
 noVNC into their products including Ganeti Web Manager, OpenStack, and
 OpenNebula.
 .
 This package contains the daemon and programs.

Package: python3-novnc
Architecture: all
Section: python
Depends:
 python3-oslo.config,
 python3-pil,
 ${misc:Depends},
 ${python3:Depends},
 ${shlibs:Depends},
Suggests:
 python3-nova,
Description: HTML5 VNC client - Python 3.x libraries
 noVNC is a HTML5 VNC (WebSockets, Canvas) with encryption (wss://) support
 client that runs well in any modern browser including mobile browsers
 (iPhone/iPad and Android). More than 16 companies/projects have integrated
 noVNC into their products including Ganeti Web Manager, OpenStack, and
 OpenNebula.
 .
 This package installs the core Python 3.x parts of NoVNC.
